﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using Riddle.Models;

namespace Riddle.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }


        public List<Riddle> Riddles { get; set; }
        [Column(TypeName = "datetime2")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public List<Review> Reviews { get; set; }
        public DateTime DateCreated { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        public DbSet<Riddle> Riddles { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<SolvedQuestion> SolvedQuestions { get; set; }
        public DbSet<PrivateMessage> PrivateMessages { get; set; }
        public DbSet<MessageThread> MessageThreads { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new RiddleConfiguration());
            modelBuilder.Configurations.Add(new ReviewConfiguration());
            modelBuilder.Configurations.Add(new QuestionConfiguration());
            base.OnModelCreating(modelBuilder);
        }

        // Entity configurations
        public class UserConfiguration: EntityTypeConfiguration<ApplicationUser>
        {
            public UserConfiguration()
            {
                Property(u => u.UserName).HasMaxLength(100).IsRequired();
            }
        }
        public class RiddleConfiguration: EntityTypeConfiguration<Riddle>
        {
            public RiddleConfiguration()
            {
                Property(r => r.Name).IsRequired().HasMaxLength(200);
                Property(r => r.Description).HasMaxLength(400);
            }
        }
        public class ReviewConfiguration: EntityTypeConfiguration<Review>
        {
            public ReviewConfiguration()
            {
                Property(r => r.Body).HasMaxLength(500);             
            }
        }
        public class QuestionConfiguration: EntityTypeConfiguration<Question>
        {
            public QuestionConfiguration()
            {
                Property(q => q.Body).IsRequired().HasMaxLength(800);
                Property(q => q.Answer).IsRequired().HasMaxLength(100);
            }
        }       
    }
}

public static class UserHelpers
{
    public static bool IsAdminUser(this System.Security.Principal.IIdentity user)
    {
        var context = new ApplicationDbContext();
        var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

        if (user != null && user.IsAuthenticated)
        {

            string userId = user.GetUserId();
            var roles = userManager.GetRoles(userId);
            foreach (var role in roles)
            {
                if (role == "Admin")
                {
                    return true;
                }
            }
        }
        return false;
    }
}