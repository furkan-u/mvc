﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Riddle.Models
{
    public class SolvedQuestion
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public int QuestionId { get; set; }
        public Question Question { get; set; }
        public int? RiddleId { get; set; }
        public Riddle Riddle { get; set; }
        public int QuestionNumber { get; set; }
    }
}