﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Riddle.Models
{
    public class Review
    {
        public int Id { get; set; }
        public string Body { get; set; }
        public int Rating { get; set; }        
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public int RiddleId { get; set; }
        public Riddle Riddle { get; set; } 
        [Column(TypeName = "datetime2")]
        public DateTime DateCreated { get; set;}

    }
}