﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Riddle.Models.App.ViewModels
{
    public class UsersSolvedViewModel
    {
        public string UserName { get; set; }
    }
}