﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Riddle.Models.App.ViewModels
{
    public class MessageThreadMessagesViewModel
    {
        public DateTime DateCreated { get; set; }
        public string ReceiverName { get; set; }
        public string SenderId { get; set; }
        public string SenderName { get; set; }
        public string MessageBody { get; set; }
    }
}