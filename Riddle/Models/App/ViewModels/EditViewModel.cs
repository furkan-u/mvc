﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Riddle.Models.App.ViewModels
{
    public class EditViewModel
    {
        public int RiddleId { get; set; }
        public int NumberOfQuestions { get; set; }

        public int QuestionId { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Edit_Question", ResourceType =typeof(Views.Riddles.Resources.Resources))]
        [MaxLength(2000)]
        public string Body { get; set; }

        [Display(Name = "Edit_Answer", ResourceType = typeof(Views.Riddles.Resources.Resources))]
        [MaxLength(100)]
        public string Answer { get; set; }

    }
}