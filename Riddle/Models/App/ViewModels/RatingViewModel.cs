﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Riddle.Models.App.ViewModels
{
    public class RatingViewModel
    {
        public double Average { get; set; }
        public int Count { get; set; }
    }
}