﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Riddle.Models.App.ViewModels
{
    public class NewMessageThreadViewModel
    {
        public string Receiver { get; set; }
        public string MessageBody { get; set; }
        public string Title { get; set; }
    }
}