﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Riddle.Models.App.ViewModels
{
    public class AddQuestionViewModel
    {
        [DataType(DataType.MultilineText)]
        [Display(Name ="Add_Question", ResourceType =typeof(Views.Question.Resources.Resources))]
        public string Body { get; set; }
        [Display(Name = "Add_Answer", ResourceType = typeof(Views.Question.Resources.Resources))]
        public string Answer { get; set; }
    }
}