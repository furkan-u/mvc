﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Riddle.Models
{
    public class CreateRiddleViewModel
    {
        [Required]
        [Display(Name="RiddleName", ResourceType = typeof(Views.Riddles.Resources.Resources))]
        public string Name { get; set; }

        [MaxLength(200)]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Create_Description", ResourceType = typeof(Views.Riddles.Resources.Resources))]
        public string Description { get; set; }
        // Question properties
        [Display(Name = "Create_FirstQuestion", ResourceType = typeof(Views.Riddles.Resources.Resources))]
        [DataType(DataType.MultilineText)]
        public string FirstQuestionBody { get; set; }

        [Display(Name = "Answer", ResourceType = typeof(Views.Riddles.Resources.Resources))]
        public string FirstQuestionAnswer { get; set; }
    }
}