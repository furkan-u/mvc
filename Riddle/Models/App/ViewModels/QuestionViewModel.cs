﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Riddle.Models.App.ViewModels
{
    public class QuestionViewModel
    {
        public Question Question { get; set; }
        public bool IsAlreadyRated { get; set; }
        public IEnumerable<string> SolvedUserNames { get; set; }
    }
}