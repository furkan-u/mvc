﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Riddle.Models.App.ViewModels
{

    public class TopRiddleViewModel{
        public double AverageRating { get; set; }
        public int VoteCount { get; set; }
        public int RiddleId { get; set; }
        public string RiddleName { get; set; }
    }

    public class TopUserViewModel
    {
        public string UserName { get; set; }
        public string Id { get; set; }
        public int QuestionCount { get; set; }
    }

    public class HomeViewModel
    {
        public IEnumerable<TopUserViewModel> Users { get; set; }
        public IEnumerable<TopRiddleViewModel> Riddles { get; set; }
    }
}