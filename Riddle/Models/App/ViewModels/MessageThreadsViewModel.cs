﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Riddle.Models.App.ViewModels
{

    public class MessageThreadsViewModel
    {
        public string Title { get; set; }
        public string LastSenderName { get; set; }
        public string LastMessageBody { get; set; }
        public int NumberOfUnreadMessages { get; set; }
        public int ThreadId { get; set; }
        public DateTime LastMessageDate { get; set; }
    }
}