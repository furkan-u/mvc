﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Riddle.Models.App.ViewModels
{
    public class SendPrivateMessageViewModel
    {
        public string ReceiverName { get; set; }
        public string SenderName { get; set; }
        public string MessageBody { get; set; }
        public int MessageThreadId { get; set; }
    }
}