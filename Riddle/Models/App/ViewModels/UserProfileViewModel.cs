﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Riddle.Models.App.ViewModels
{
    public class UserProfileViewModel
    {
        public string ShownUserName { get; set; }
        public string CurrentUserId { get; set; }
        public int NumberOfQuestionsSolved { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string ShownUserId { get; set; }

    }
}