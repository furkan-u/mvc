﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Riddle.Models
{
    public class Question
    {
        public int Id { get; set; }
        [Display(Name = "Question", ResourceType = typeof(Views.Riddles.Resources.Resources))]
        [MaxLength(2000)]
        [DataType(DataType.MultilineText)]
        public string Body { get; set; }
        [Display(Name = "Answer", ResourceType = typeof(Views.Riddles.Resources.Resources))]
        public string Answer { get; set; }
        public int RiddleId { get; set; }
        public Riddle Riddle { get; set; }

        /*
          
          if the Riddle is not in memory, this would require you to first execute a
            query on the database to retrieve that Riddle so that you can set the property. There
            are times when you may not have the object in memory, but you do have access to that
            object’s key value. With a foreign key property, you can simply use the key value with-
            out depending on having that instance in memory. Also, the relationship is know required
            since the RiddleId is an int and therefore non-nullable. That's why we have foreign key here.
          */
        
        
        [Column(TypeName = "datetime2")]
        public DateTime DateCreated { get; set; }
        public int QuestionNumber { get; set; }
    }
}