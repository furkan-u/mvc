﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNet.Identity;

namespace Riddle.Models
{
    public class Riddle
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Display(Name = "Riddles_RiddleName", ResourceType =typeof(Views.Manage.Resources.Resources))]
        public string Name { get; set; }

        [MaxLength(200)]
        [DataType(DataType.MultilineText)]

        [Display(Name = "Riddles_Description", ResourceType = typeof(Views.Manage.Resources.Resources))]
        public string Description { get; set; }
        public string UserId { get; set; }
        [Required]
        public virtual ApplicationUser User { get; set; }
        
        [Column(TypeName = "datetime2")]

        [Display(Name = "Riddles_DateCreated", ResourceType = typeof(Views.Manage.Resources.Resources))]
        public DateTime DateCreated { get; set; }

        public virtual List<Review> Reviews { get; set; }
        public virtual List<Question> Questions { get; set; }

    }


}


