﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Riddle.Models
{
    public class MessageThread
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ReceiverId { get; set; }
        public string SenderId { get; set; }
        public List<PrivateMessage> Messages { get; set; }
    }
}