﻿using System;

namespace Riddle.Models
{
    public class PrivateMessage
    {
        public int Id { get; set; }
        public string Body { get; set; }
        public string SenderId { get; set; }
        public string ReceiverId { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsRead { get; set; }
        public int MessageThreadId { get; set; }
        public MessageThread MessageThread { get; set; }
    }
}