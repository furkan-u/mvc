namespace Riddle.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Riddle.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Riddle.Models.ApplicationDbContext context)
        {

            if (!context.Users.Any(u => u.UserName == "Furkan") && !context.Users.Any(u => u.UserName == "Test"))
            {
                var store = new UserStore<Models.ApplicationUser>(context);
                var manager = new UserManager<Models.ApplicationUser>(store);

                var furkan = new Models.ApplicationUser { UserName = "Furkan", DateCreated = DateTime.UtcNow };
                var test = new Models.ApplicationUser { UserName = "Test", DateCreated = DateTime.UtcNow };
                manager.Create(furkan, "password");
                manager.Create(test, "password");

                for (int i = 0; i < 1000; i++)
                {
                    context.Riddles.AddOrUpdate(r => r.Name,
                        new Models.Riddle
                        {
                            Name = i.ToString(),
                            User = manager.FindByName("Furkan"),
                            DateCreated = DateTime.Now,
                            Description = "Description of Riddle" + i,
                            Questions = new System.Collections.Generic.List<Models.Question>
                            {
                                new Models.Question
                                {
                                    Body = "Question " + 1 + " Body",
                                    Answer = "answer",
                                    DateCreated = DateTime.Now,
                                    QuestionNumber = 1
                                },
                                new Models.Question
                                {
                                    Body = "Question " + 2 + " Body",
                                    Answer = "answer",
                                    DateCreated = DateTime.Now,
                                    QuestionNumber = 2
                                },
                                new Models.Question
                                {
                                    Body = "Question " + 3 + " Body",
                                    Answer = "answer",
                                    DateCreated = DateTime.Now,
                                    QuestionNumber = 3
                                },
                                new Models.Question
                                {
                                    Body = "Question " + 4 + " Body",
                                    Answer = "answer",
                                    DateCreated = DateTime.Now,
                                    QuestionNumber = 4
                                }
                            }

                        });
                }

            }
        }
    }
}
