﻿using Microsoft.AspNet.Identity;
using Riddle.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Riddle.Models.App.ViewModels;
using X.PagedList;


namespace Riddle.Controllers
{

    [Authorize]
    public class RiddlesController : Controller
    {


        private ApplicationDbContext _db = new ApplicationDbContext();



        public ActionResult Index(string searchTerm = "", int page = 1)
        {
            var model = _db.Riddles.Where(r => searchTerm == "" || r.Name.StartsWith(searchTerm))
                                   .OrderByDescending(r => r.DateCreated)
                                   .Include("User")
                                   .ToPagedList(page, 10);

            if (Request.IsAjaxRequest())
            {
                return PartialView("_Riddles", model);
            }
            return View(model);
        }

        public ActionResult Autocomplete(string term = "")
        {
            var model = _db.Riddles.Where(r => r.Name.StartsWith(term))
                                   .Take(10)
                                   .OrderBy(r => r.DateCreated)
                                   .Select(r => new
                                   {
                                       label = r.Name
                                   });


            return Json(model, JsonRequestBehavior.AllowGet);
        }


        
        public ActionResult Riddle(int Id, int page = 1) // page değişkeni sadece RiddleOwner View'i için kullanılacak.
        {
            string currentUserId = User.Identity.GetUserId();
            var riddle = _db.Riddles.Where(r => r.Id == Id).Single();

            if (riddle.User.Id == User.Identity.GetUserId()) // Check whether current user created this riddle. If so, send him/her to RiddleOwner view.
            {
                var model = riddle.Questions
                                  .Select(q => new QuestionViewModel
                                  {
                                      Question = q,
                                      SolvedUserNames = from s in _db.SolvedQuestions
                                                        where s.QuestionId == q.Id
                                                        select s.User.UserName,
                                  })
                                  .ToPagedList(page, 1);
                if (Request.IsAjaxRequest())
                {
                    return PartialView("_RiddleOwner", model);
                }
                return View("RiddleOwner", model);

            }
            else
            {
                // Check if whether already rated
                bool isAlreadyRated = _db.Reviews
                                         .Any(r => r.User.Id == currentUserId && r.Riddle.Id == Id);


                int lastQuestionId = _db.Questions
                                        .Where(q => q.RiddleId == Id)
                                        .OrderByDescending(q => q.QuestionNumber)
                                        .First().Id;
                bool isRiddleCompleted = _db.SolvedQuestions
                                            .Any(s => s.QuestionId == lastQuestionId && currentUserId == s.UserId);
                if (!isRiddleCompleted)
                {
                    var lastSolvedQuestion = _db.SolvedQuestions
                                                .Where(q => q.UserId == currentUserId && q.RiddleId == Id)
                                                .OrderByDescending(q => q.QuestionNumber)
                                                .FirstOrDefault();
                    if (lastSolvedQuestion == null) // If user hasn't solved any questions, return first question
                    {
                        var model = riddle.Questions
                                          .Where(q => q.QuestionNumber == 1)
                                          .Select(q => new QuestionViewModel
                                          {
                                              IsAlreadyRated = isAlreadyRated,
                                              Question = q,
                                              SolvedUserNames = from s in _db.SolvedQuestions
                                                                where s.QuestionId == q.Id
                                                                select s.User.UserName
                                          })
                                          .Single();

                        return View(model);
                    }

                    else // return next question
                    {
                        var model = riddle.Questions
                                          .Where(q => q.QuestionNumber == lastSolvedQuestion.QuestionNumber + 1)
                                          .Select(q => new QuestionViewModel
                                          {
                                              IsAlreadyRated = isAlreadyRated,
                                              Question = q,
                                              SolvedUserNames = from s in _db.SolvedQuestions
                                                                where s.QuestionId == q.Id
                                                                select s.User.UserName
                                          })
                                          .Single();

                        return View(model);
                    }
                }
                else
                {
                    var questions = riddle.Questions
                                          .OrderBy(q => q.QuestionNumber)
                                          .Select(q => new QuestionViewModel
                                          {
                                              IsAlreadyRated = isAlreadyRated,
                                              Question = q,
                                              SolvedUserNames = from s in _db.SolvedQuestions
                                                                where s.QuestionId == q.Id
                                                                select s.User.UserName,

                                          })
                                          .ToPagedList(page, 1);
                    return View("SolvedRiddle", questions);
                }
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Riddle([Bind(Prefix ="id")]int riddleId, string answer, int questionNumber = 1) // Bu kısım ajax kullanılarak düzenlenebilir. Böylece cevap verdikten sonra sayfa yenilemesine gerek kalmaz.
        {
            var riddle = _db.Riddles.Where(r => r.Id == riddleId).Single();
            string currentUserId = User.Identity.GetUserId();
            if (riddle.User.Id == currentUserId)
            {
                var question = riddle.Questions.Where(q => q.QuestionNumber == questionNumber).SingleOrDefault();
                return View("RiddleOwner", question);
            }
            else
            {
                bool isCorrect = answer == riddle.Questions.Where(q => q.Riddle.Id == riddleId && q.QuestionNumber == questionNumber).Single().Answer;
                ViewBag.IsCorrect = isCorrect;
                ViewBag.RiddleId = riddleId;
                if (isCorrect)
                {
                    int questionId = riddle.Questions.Where(q => q.QuestionNumber == questionNumber).Single().Id;
                    _db.SolvedQuestions.Add(new Riddle.Models.SolvedQuestion
                    {
                        QuestionId = questionId,
                        UserId = User.Identity.GetUserId(),
                        QuestionNumber = questionNumber,
                        RiddleId = riddleId
                    });
                    _db.SaveChanges();
                    return PartialView("_AnswerResult");
                }
                return PartialView("_AnswerResult");
            }
        }
        
        public ActionResult Create()
        {
            return View();
        }

      
        

        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(CreateRiddleViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _db.Questions.Add(new Models.Question
                    {
                        Body = model.FirstQuestionBody,
                        Answer = model.FirstQuestionAnswer,
                        DateCreated = DateTime.Now,
                        QuestionNumber = 1,
                        Riddle = new Models.Riddle
                        {
                            Name = model.Name,
                            Description = model.Description,
                            DateCreated = DateTime.Now,
                            User = _db.Users.Find(User.Identity.GetUserId()),
                        }
                    });

                    _db.SaveChanges();
                    return RedirectToAction("Index");
                }

                catch
                {
                    return View();
                }
            }
            return View();
        }

        public ActionResult Edit([Bind(Prefix = "Id")] int RiddleId, int page = 1)
        {
            var riddle = _db.Riddles.Where(r => r.Id == RiddleId).Single();
            if (riddle.User.Id == User.Identity.GetUserId())
            {
                var model = riddle.Questions
                                  .Select(q => new EditViewModel
                                  {
                                      RiddleId = riddle.Id,
                                      NumberOfQuestions = riddle.Questions.Count(),
                                      Body = q.Body,
                                      Answer = q.Answer,
                                      QuestionId = q.Id
                                  })
                                  .ToPagedList(page, 1);
                return View(model);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult EditRiddle([Bind(Prefix = "id")] int RiddleId, int QuestionId, EditViewModel viewModel, int page = 1)
        {
            if (ModelState.IsValid)
            {
                var question = _db.Questions
                                  .Where(q => q.Id == QuestionId)
                                  .Single();
                question.Body = viewModel.Body;
                question.Answer = viewModel.Answer;
                _db.SaveChanges();
                return RedirectToAction("Edit", new { Id = RiddleId, page = question.QuestionNumber });
            }


            return View(viewModel);
        }



        public ActionResult Add([Bind(Prefix = "id")]int RiddleId)
        {
            ViewBag.RiddleId = RiddleId;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Add([Bind(Prefix = "id")]int RiddleId, AddQuestionViewModel question)
        {
            var riddle = _db.Riddles.Where(r => r.Id == RiddleId).Single();
            var questions = riddle.Questions;
            int NumberOfQuestions = riddle.Questions.Count();

            questions.Add(new Question
            {
                Body = question.Body,
                Answer = question.Answer,
                DateCreated = DateTime.Now,
                QuestionNumber = NumberOfQuestions + 1,
                Riddle = riddle
            });
            _db.SaveChanges();
            return RedirectToAction("Edit", new { Id = RiddleId, questionNumber = NumberOfQuestions });
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (_db != null)
            {
                _db.Dispose();
            }
        }


    }
}
