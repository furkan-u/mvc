﻿using Microsoft.AspNet.Identity;
using Riddle.Models;
using Riddle.Models.App.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Riddle.Controllers
{
    public class RatingsController : Controller
    {

        ApplicationDbContext _db = new ApplicationDbContext();
        public ActionResult NewRating(int Rating, int riddleId)
        {
            string userId = User.Identity.GetUserId();
            // check if already rated
            var original = _db.Reviews
                              .Where(r => r.User.Id == userId && r.Riddle.Id == riddleId)
                              .SingleOrDefault();
            if (original != null)
            {
                original.Rating = Rating;
                _db.SaveChanges();
            }
            else
            {
                _db.Reviews.Add(new Review
                {
                    Rating = Rating,
                    DateCreated = DateTime.Now,
                    RiddleId = riddleId,
                    UserId = userId
                });
                _db.SaveChanges();
            }


            var reviews = _db.Riddles
                             .Where(r => r.Id == riddleId)
                             .Single()
                             .Reviews
                             .AsEnumerable();
            var model = new RatingViewModel
            {
                Average = reviews.Average(r => r.Rating),
                Count = reviews.Count()
            };
         
            return PartialView("_Rating", model);
    }


    protected override void Dispose(bool disposing)
    {
        base.Dispose(disposing);
        if (_db != null)
        {
            _db.Dispose();
        }
    }
}
}