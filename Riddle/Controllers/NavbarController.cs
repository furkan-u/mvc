﻿using Microsoft.AspNet.Identity;
using Riddle.Models;
using Riddle.Models.App.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Riddle.Controllers
{
    public class NavbarController : Controller
    {
        ApplicationDbContext _db = new ApplicationDbContext();

        [ChildActionOnly]
        public ActionResult Index()
        {
            string currentUserId = User.Identity.GetUserId();
            var model = new NavbarViewModel
            {
               
                NumberOfUnreadMessages = _db.PrivateMessages
                                             .Where(p => p.ReceiverId == currentUserId && p.IsRead == false)
                                             .Count()
            };
            return PartialView("_NavbarPartial", model);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (_db != null)
            {
                _db.Dispose();
            }
        }
    }
}