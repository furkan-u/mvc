﻿using Microsoft.AspNet.Identity;
using X.PagedList;
using Riddle.Models;
using Riddle.Models.App.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Riddle.Controllers
{
    public class QuestionController : Controller
    {
        ApplicationDbContext _db = new ApplicationDbContext();
        public ActionResult UsersSolved([Bind(Prefix = "id")]int QuestionId, int page = 1)
        {
            var model =  from u in _db.Users
                         join s in _db.SolvedQuestions on u.Id equals s.UserId
                         where s.QuestionId == QuestionId
                         orderby u.DateCreated
                         select new UsersSolvedViewModel
                         {
                             UserName = u.UserName
                         };     
                                  
            return View(model.ToPagedList(page,30));
        }

        public ActionResult Add([Bind(Prefix="id")] int RiddleId)
        {
            ViewBag.RiddleId = RiddleId;
            string ownerId = _db.Riddles
                                .Where(r => r.Id == RiddleId)
                                .Select(r => r.UserId)
                                .Single();
            if (User.Identity.GetUserId() != ownerId)
                return HttpNotFound();     
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add([Bind(Prefix ="id")] int RiddleId, AddQuestionViewModel model)
        {
            if (ModelState.IsValid)
            {
                int lastQuestionNumber = _db.Questions
                                            .Where(q => q.RiddleId == RiddleId)
                                            .OrderByDescending(q => q.QuestionNumber)
                                            .Select(q=> q.QuestionNumber)
                                            .First();
                _db.Questions
                   .Add(new Question
                   {
                       Body = model.Body,
                       Answer = model.Answer,
                       DateCreated = DateTime.Now,
                       QuestionNumber = lastQuestionNumber+1,
                       RiddleId = RiddleId

                   });
                _db.SaveChanges();
                return RedirectToAction("Edit", "Riddles", new { Id = RiddleId, page = lastQuestionNumber + 1 });
            }
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (_db != null)
                _db.Dispose();
        }
    }
}