﻿using Microsoft.AspNet.Identity;
using X.PagedList;
using Riddle.Models;
using Riddle.Models.App.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Riddle.Controllers
{
    public class PrivateMessageController : Controller
    {

        ApplicationDbContext _db = new ApplicationDbContext();


        // GET: PrivateMessage
        [Authorize]
        public ActionResult Index(int page = 1) // You need group join here.
        {
            string currentUserId = User.Identity.GetUserId();
            var model = GetMessageThreads(page);

            if (Request.IsAjaxRequest())
                return PartialView("_MessageThreads", model);

            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult NewMessageThread(NewMessageThreadViewModel viewModel, int page = 1)
        {
            string currentUserId = User.Identity.GetUserId();
            string receiverId = _db.Users.Where(u => u.UserName == viewModel.Receiver).Single().Id;
            var firstMessage = new PrivateMessage
            {
                SenderId = currentUserId,
                ReceiverId = receiverId,
                Body = viewModel.MessageBody,
                IsRead = false,
                DateCreated = DateTime.Now
            };
            _db.MessageThreads.Add(new MessageThread
            {
                Messages = new List<PrivateMessage>
                {
                    firstMessage
                },
                Title = viewModel.Title,
                SenderId = currentUserId,
                ReceiverId = receiverId
            });
            _db.SaveChanges();
            if (Request.IsAjaxRequest())
            {
                var model = GetMessageThreads(page);

                return PartialView("_MessageThreads", model);
            }
            else
            {
                return RedirectToAction("Index");
            }



        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult NewPrivateMessage(SendPrivateMessageViewModel pmViewModel, int page = -1)
        {
            string currentUserId = User.Identity.GetUserId();
            string currentUserName = _db.Users.Find(currentUserId).UserName;
            pmViewModel.ReceiverName = currentUserName == pmViewModel.SenderName ? pmViewModel.ReceiverName : pmViewModel.SenderName;

            if (Request.IsAjaxRequest())
            {
                _db.PrivateMessages.Add(new PrivateMessage
                {
                    Body = pmViewModel.MessageBody,
                    DateCreated = DateTime.Now,
                    IsRead = false,
                    MessageThreadId = pmViewModel.MessageThreadId,
                    ReceiverId = _db.Users.Where(u => u.UserName == pmViewModel.ReceiverName).FirstOrDefault().Id,
                    SenderId = currentUserId
                });
                _db.SaveChanges();


                var messages = _db.PrivateMessages.Where(pm => pm.MessageThreadId == pmViewModel.MessageThreadId);
                var model = GetMessageThreadMessages(messages, page);
                SetReceiverNameAndId(pmViewModel.MessageThreadId, currentUserName, model);

                ViewBag.MessageThreadId = pmViewModel.MessageThreadId;
                return PartialView("_MessageThreadMessages", model);
            }
            return HttpNotFound();
        }

        public ActionResult MessageThreadMessages(int MessageThreadId, int page = -1)
        {
            if (Request.IsAjaxRequest())
            {
                string currentUserId = User.Identity.GetUserId();
                string currentUserName = User.Identity.GetUserName();
                var messages = _db.PrivateMessages.Where(pm => pm.MessageThreadId == MessageThreadId);
                foreach (var item in messages.Where(m => m.IsRead == false && m.ReceiverId == currentUserId))
                    item.IsRead = true;
                _db.SaveChanges();
                var model = GetMessageThreadMessages(messages, page);
                SetReceiverNameAndId(MessageThreadId, currentUserName, model);


                return PartialView("_MessageThreadMessages", model);
            }
            return HttpNotFound();
        }

        private IPagedList<MessageThreadMessagesViewModel> GetMessageThreadMessages(IQueryable<PrivateMessage> messages, int page)
        {
            var nonPageListModel = from pm in messages
                                   orderby pm.DateCreated ascending
                                   let Sender = _db.Users.Where(u => u.Id == pm.SenderId).FirstOrDefault()
                                   select new MessageThreadMessagesViewModel
                                   {
                                       MessageBody = pm.Body,
                                       SenderName = Sender.UserName,
                                       SenderId = Sender.Id,
                                       ReceiverName = _db.Users.Where(u => u.Id == pm.ReceiverId).FirstOrDefault().UserName,
                                       DateCreated = pm.DateCreated
                                   };
            if (page == -1)
            {
                page = (int)Math.Ceiling(nonPageListModel.Count() / 6.0);
            }
            var model = nonPageListModel.ToPagedList(page, 6);

            return model;
        }

        private IPagedList<MessageThreadsViewModel> GetMessageThreads(int page)
        {
            string currentUserId = User.Identity.GetUserId();
            IPagedList<MessageThreadsViewModel> model = (from pm in _db.PrivateMessages.Where(p => p.ReceiverId == currentUserId || p.SenderId == currentUserId)
                                                         group pm by pm.MessageThread into pmg
                                                         let lastMessage = pmg.OrderByDescending(p => p.DateCreated).FirstOrDefault()
                                                         let lastMessageDate = lastMessage.DateCreated
                                                         let lastSenderName = _db.Users.Where(u => lastMessage.SenderId == u.Id).FirstOrDefault().UserName
                                                         let lastMessageBody = lastMessage.Body
                                                         orderby lastMessageDate descending
                                                         select new MessageThreadsViewModel
                                                         {
                                                             NumberOfUnreadMessages = pmg.Key.Messages.Count(m => m.IsRead == false && m.ReceiverId == currentUserId),
                                                             Title = pmg.Key.Title,
                                                             LastSenderName = lastSenderName,
                                                             ThreadId = pmg.Key.Id,
                                                             LastMessageDate = lastMessageDate,
                                                             LastMessageBody = lastMessageBody.Substring(0, 50)
                                                         }).ToPagedList(page, 10);

            return model;
        }

        private void SetReceiverNameAndId(int messageThreadId, string currentUserName, IPagedList<MessageThreadMessagesViewModel> model)
        {
            ViewBag.MessageThreadId = messageThreadId;
            string receiverName = currentUserName != model.First().ReceiverName ? model.First().ReceiverName : model.First().SenderName; // find the receiver name relative to the current user.
            ViewBag.ReceiverId = _db.Users.Where(u => u.UserName == receiverName).First().Id; // find the receiver id relative to the current user.
            ViewBag.ReceiverName = receiverName;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (_db != null)
            {
                _db.Dispose();
            }
        }
    }
}