﻿using Riddle.Models;
using Riddle.Models.App.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Riddle.Controllers
{

    public class HomeController : Controller
    {
        ApplicationDbContext _db = new ApplicationDbContext();
        public ActionResult Index()
        {

            var groupUserQuery = from q in _db.SolvedQuestions
                                 group q by q.UserId into grouping
                                 orderby grouping.Count() descending
                                 select new
                                 {
                                     Count = grouping.Count(),
                                     UserId = grouping.Key
                                 };

            var topUsers = (from u in _db.Users
                            join gq in groupUserQuery on u.Id equals gq.UserId
                            orderby gq.Count descending
                            select new TopUserViewModel
                            {
                                QuestionCount = gq.Count,
                                UserName = u.UserName,
                                Id = u.Id
                            }).Take(5);

            var topRiddles = (from r in _db.Reviews
                              group r by r.RiddleId into grouping
                              let avg = grouping.Average(g => g.Rating)
                              orderby avg descending
                              select new TopRiddleViewModel
                              {
                                  RiddleId = grouping.Key,
                                  AverageRating = avg,
                                  RiddleName = _db.Riddles
                                                  .Where(r => r.Id == grouping.Key)
                                                  .FirstOrDefault().Name,
                                  VoteCount = grouping.Count()
                              }).Take(5);

            var model = new HomeViewModel
            {
                Users = topUsers,
                Riddles = topRiddles
            };
            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult ChangeLanguage([Bind(Prefix ="lang")]string languageAbbreviation,string returnUrl)
        {
            if (languageAbbreviation != null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageAbbreviation);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageAbbreviation);
            }
            HttpCookie cookie = new HttpCookie("Language");
            cookie.Value = languageAbbreviation;
            Response.Cookies.Add(cookie);

            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");

        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (_db != null)
                _db.Dispose();
        }


    }
}