﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Riddle.Models;
using Riddle.Models.App.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using X.PagedList;

namespace Riddle.Controllers
{
    public class AdminController : Controller
    {


        ApplicationDbContext _db = new ApplicationDbContext();
        
        [Authorize]
        public ActionResult Index()
        {   
            if (isAdminUser())
            {
                return View();
            }
            return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            
        }


        [Authorize]
        public ActionResult EditRiddles(string searchTerm = "", int page = 1)
        {
            var model = _db.Riddles.Where(r => searchTerm == "" || r.Name.StartsWith(searchTerm))
                                   .OrderByDescending(r => r.DateCreated)
                                   .Include("User")
                                   .ToPagedList(page, 10);

            if (Request.IsAjaxRequest())
            {
                return PartialView("_Riddles", model);
            }
            return View(model);
        }

        [Authorize]
        public ActionResult EditRiddle([Bind(Prefix = "Id")] int RiddleId, int page = 1)
        {
            var riddle = _db.Riddles.Where(r => r.Id == RiddleId).Single();
            if (isAdminUser())
            {
                var model = riddle.Questions
                                  .Select(q => new EditViewModel
                                  {
                                      RiddleId = riddle.Id,
                                      NumberOfQuestions = riddle.Questions.Count(),
                                      Body = q.Body,
                                      Answer = q.Answer,
                                      QuestionId = q.Id
                                  })
                                  .ToPagedList(page, 1);
                return View(model);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditRiddle([Bind(Prefix = "id")] int RiddleId, int QuestionId, EditViewModel viewModel, int page = 1)
        {
            if (ModelState.IsValid)
            {
                var question = _db.Questions
                                  .Where(q => q.Id == QuestionId)
                                  .Single();
                question.Body = viewModel.Body;
                question.Answer = viewModel.Answer;
                _db.SaveChanges();
                return RedirectToAction("EditRiddle", new { Id = RiddleId, page = question.QuestionNumber });
            }
            return View(viewModel);
        }

        [Authorize]
        public ActionResult DeleteRiddle(int id)
        {
            if (isAdminUser())
            {
                var riddle = _db.Riddles.Find(id);
                _db.Riddles.Remove(riddle);
                _db.SaveChanges();
                return RedirectToActionPermanent("EditRiddles");
            }
            return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        private bool isAdminUser()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ApplicationDbContext context = new ApplicationDbContext();
                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

                var roles = userManager.GetRoles(user.GetUserId());
                foreach(var item in roles)
                {
                    if (item.ToString() == "Admin")
                        return true;
                }
                return false;         
            }

            return false;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if(_db != null){
                _db.Dispose();
            }
        }

    }
}