﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Riddle.Models;
using Riddle.Models.App.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Riddle.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        ApplicationDbContext _db = new ApplicationDbContext();
        public new ActionResult Profile(string id)
        {
            string currentUserId = User.Identity.GetUserId();
            var store = new UserStore<Models.ApplicationUser>(_db);
            var manager = new UserManager<Models.ApplicationUser>(store);
            var user = manager.FindById(id);
            int NumberOfQuestionsSolved = _db.SolvedQuestions
                                            .Where(s => s.UserId == id)
                                            .Count();
            DateTime registrationDate = user.DateCreated;
            var model = new UserProfileViewModel
            {
                RegistrationDate = registrationDate,
                NumberOfQuestionsSolved = NumberOfQuestionsSolved,
                CurrentUserId = currentUserId,
                ShownUserName = user.UserName,
                ShownUserId = id
            };

            return View(model);
        }

        public ActionResult AutocompleteUser(string term)
        {
            var model = _db.Users
                           .Where(u => u.UserName.StartsWith(term))
                           .Take(5)
                           .Select(u => new
                           {
                               label = u.UserName
                           });
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (_db != null)
            {
                _db.Dispose();
            }
        }
    }
}