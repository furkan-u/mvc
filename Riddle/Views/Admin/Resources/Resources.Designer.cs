﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Riddle.Views.Admin.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Riddle.Views.Admin.Resources.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Düzenle.
        /// </summary>
        public static string EditRiddle_Edit {
            get {
                return ResourceManager.GetString("EditRiddle_Edit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Silmek istediğinizden emin misiniz?.
        /// </summary>
        public static string EditRiddles_AreYouSureYouWantToDelete {
            get {
                return ResourceManager.GetString("EditRiddles_AreYouSureYouWantToDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sil.
        /// </summary>
        public static string EditRiddles_Delete {
            get {
                return ResourceManager.GetString("EditRiddles_Delete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hayır.
        /// </summary>
        public static string EditRiddles_No {
            get {
                return ResourceManager.GetString("EditRiddles_No", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Evet.
        /// </summary>
        public static string EditRiddles_Yes {
            get {
                return ResourceManager.GetString("EditRiddles_Yes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Admin Paneli.
        /// </summary>
        public static string Index_AdminPanel {
            get {
                return ResourceManager.GetString("Index_AdminPanel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yönetim.
        /// </summary>
        public static string Index_Management {
            get {
                return ResourceManager.GetString("Index_Management", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bilmeceleri Düzenle/Sil.
        /// </summary>
        public static string Index_ManageRiddles {
            get {
                return ResourceManager.GetString("Index_ManageRiddles", resourceCulture);
            }
        }
    }
}
