﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using Riddle.Models;
using System;

[assembly: OwinStartupAttribute(typeof(Riddle.Startup))]
namespace Riddle
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            createRolesAndUsers();
        }

        private void createRolesAndUsers()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            // add admin role if not exists
            if (!roleManager.RoleExists("Admin"))
            {
                var role = new IdentityRole();
                role.Name = "Admin";
                roleManager.Create(role);
            }

            // create admin user
            var user = new ApplicationUser {Email="Admin@hotmail.com", UserName = "Admin", DateCreated = DateTime.Now};
            var check = userManager.Create(user, "adminpassword");

            // if user added successfully, add him to admin role
            if (check.Succeeded)
            {
                userManager.AddToRole(user.Id, "Admin");
            }



        }
    }
}
