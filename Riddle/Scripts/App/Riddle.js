﻿$(function () {


    //////////////////////////////
    // Ajax code for asynchronous searh
    //////////////////////////////
    var ajaxSubmitHandler = function () {
        var form = $(this);
        var options = {
            type: form.attr("method"),
            url: form.attr("action"),
            data: form.serialize()
        };

        $.ajax(options).done(function (data) {

            var $target = $(form.attr("data-riddle-target"));
            var $newHtml = $(data);            
            $target.replaceWith($newHtml);
            $newHtml.effect("highlight");
        });
        return false;
    };
    $("form[data-riddle-ajax='true']").submit(ajaxSubmitHandler);

    //////////////////////////////
    //code for autocomplete widget
    //////////////////////////////
    var submitAutocompleteForm = function (event, ui) {
        var input = $(this);
        input.val(ui.item.label);

        var form = input.parents("form:first");
        form.submit();
    };

    var createAutocomplete = function () {
        var input = $(this);
        var options = {
           source: input.attr("data-riddle-autocomplete"),
           select: submitAutocompleteForm
        };

        input.autocomplete(options);
    };
    $("input[data-riddle-autocomplete]").each(createAutocomplete);


    ////////////////////////////////////
    //code for asynchronous paging
    ////////////////////////////////////


    var getPage = function () {
        $a = $(this);
        var options = {
            url: $a.attr("href"),
            data: $("form").serialize(),
            type: "get"
        };

        $.ajax(options).done(function (data) {
            var target = $a.parents("div.pagedList").attr("data-riddle-target");
            $(target).replaceWith(data);
        });

        return false;
    };

    $(".body-content").on("click", ".pagedList a", getPage);

    $.fn.exists = function () {
        return this.length !== 0;
    }

});


