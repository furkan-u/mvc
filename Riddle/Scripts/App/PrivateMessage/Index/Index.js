﻿$(function () {

    var createAutocomplete = function () {
        var options = {
            source: $(this).attr("data-riddle-autocomplete")
        };
        $(this).autocomplete(options);
    };
    var newMessageThreadAjax = function () {
        $form = $($(this).attr("data-riddle-ajax-form"));
        $button = $(this)

        var options = {
            url: $form.attr("action"),
            type: $form.attr("method"),
            data: $form.serialize(),
            beforeSend: function () {
                $("button[data-dismiss='modal']").trigger('click');
                $("#messageThreads, #messageThreadMessages").hide();
                $("#loadingForThreads").show();
                $form.find("input[type='text'], textarea").val("");
            }
        }
        $.ajax(options).done(function (data) {
            var $target = $($form.attr("data-riddle-ajax-target"));
            $("#loadingForThreads").hide();
            $target.replaceWith(data);

        });

        return false;
    };
    var getMessagesAjax = function () {
        $(this).removeAttr("data-hasUnreadMessages");
        $(this).siblings(".list-group-message-thread-item").removeClass("active");
        $(this).addClass("active").find("span.badge").remove();
        $target = $($(this).attr("data-riddle-ajax-target"));
        var options = {
            url: $(this).attr("data-riddle-ajax-action"),
            type: $(this).attr("data-riddle-ajax-method"),
            beforeSend: function () {
                $("#messageThreadMessages").hide();
                $("#loadingForMessages").show();
            }
        };

        $.ajax(options).done(function (data) {
            $("#loadingForMessages").hide();
            $target.replaceWith(data);
        });

        return false;
    };
    var newMessageAjax = function () {
        $form = $(this);
        var options = {
            url: $form.attr("action"),
            type: $form.attr("method"),
            data: $form.serialize(),
            beforeSend: function () {
                $form.children("button[type='submit']").text("Gönderiliyor..");
            }
        };
        $.ajax(options).done(function (data) {
            $target = $($form.attr("data-riddle-ajax-target"));
            $target.replaceWith(data);
            
        });

        return false;
    };

    var getPageAjax = function () {
        $target = $($(this).parents(".pagedListPager:first").attr("data-riddle-ajax-target"));
        var options = {
            url: $(this).attr("href"),
            type: "get"
        };
        $.ajax(options).done(function (data) {
            $target.replaceWith(data);
        });

        return false;
    };
    var getMessagesWithPagerAjax = function () {
        $target = $($(this).parents("#messagesPager").attr("data-riddle-ajax-target"));
        var options = {
            url: $(this).attr("href"),
            type: "get"
        };
        $.ajax(options).done(function (data) {
            $target.replaceWith(data);
        });
        return false;
    };


    $(".body-content").on("click", ".list-group-message-thread-item", getMessagesAjax);
    $(".body-content").on("click", ".modal .modal-footer button[data-riddle-ajax='true']", newMessageThreadAjax);
    $(".body-content").on("submit", "form#newMessageForm", newMessageAjax);
    $(".body-content").on("click", ".pagedListPager a", getPageAjax);
    $(".body-content").on("click", "#messagesPager a", getMessagesWithPagerAjax);
    $("input[data-riddle-autocomplete]").each(createAutocomplete);

    $(".body-content").on('click', ".BBbutton", function () {
        var tag = $(this).attr("data-riddle-bbtag");
        $textArea = $(this).nextAll(".form-group").find("textarea");
        var start = $textArea[0].selectionStart;
        var end = $textArea[0].selectionEnd;
        var textAreaTxt = $textArea.val();
        var startTag = "";
        var endTag = "";
        switch (tag) {
            case "b":
                startTag = "[b]";
                endTag = "[/b]";
                break;
            case "i":
                startTag = "[i]";
                endTag = "[/i]";
                break;
            case "u":
                startTag = "[u]";
                endTag = "[/u]";
                break;
            case "quote":
                startTag = "[quote]";
                endTag = "[/quote]";
                break;
            case "link":
                startTag = "[url=]";
                endTag = "[/url]";
                break;
            case "image":
                startTag = "[image]";
                endTag = "[/image]";
                break;
            case "simage":
                startTag = "[simage]";
                endTag = "[/simage]";
                break;
            case "code":
                startTag = "[code]";
                endTag = "[/code]";
                break;
            case "center":
                startTag = "[center]";
                endTag = "[/center]";
                break;
            default:
        }
        $textArea.val(textAreaTxt.substring(0, start) + startTag + textAreaTxt.substring(start, end) + endTag + textAreaTxt.substring(end));
    });
});