﻿$(function () {

    var riddleOwnerPager = function () {
   
        var options = {
            url: $(this).attr("href"),
            type: "get"
        };
        $.ajax(options).done(function (data) {
            var target = $("div.pagedList").attr("data-riddle-target");
            $(target).replaceWith(data);
        });
        return false;
    };

    $(".body-content").on("click", ".pagedList a", riddleOwnerPager);

});