﻿$(function () {

    var displayLoadingGif = function(){
        $form.hide();
        $("#rating #loading").css('display', 'block');        
    }

    var sendAjaxRequest = function () {
        $form = $(this);        
        var options = {
            url: $form.attr("action"),
            type: $form.attr("method"),
            data: $form.serialize(),
            beforeSend: displayLoadingGif
        };

        $.ajax(options).done(function (data) {
            var $target = $($form.attr("data-riddle-target"));
            $target.replaceWith(data);
        });
        return false;
    };

    $form = $("form[data-riddle-ajax='rating']");
    $form.on("submit", sendAjaxRequest);
});