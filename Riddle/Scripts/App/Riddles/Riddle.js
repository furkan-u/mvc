﻿$(function () {
    var sendAnswerAjax = function () {

        $form = $(this).parents("form:first");
        $target = $($(this).attr("data-riddle-target"));

        var options = {
            url: $form.attr("action"),
            type: $form.attr("method"),
            data: $form.serialize(),
            beforeSend: function () {
                $(this).val("Gönderiliyor..");
            }
        };

        $.ajax(options).done(function (data) {
               $target.html(data);
        });
        return false;
    };



    $(".body-content").on("click", "button#sendAnswer", sendAnswerAjax);
});