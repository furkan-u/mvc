﻿$(function () {
    $(".body-content").on('click', ".BBbutton", function () {
        var tag = $(this).attr("data-riddle-bbtag");
        $textArea = $('textarea#questionBody');
        var start = $textArea[0].selectionStart;
        var end = $textArea[0].selectionEnd;
        var textAreaTxt = $textArea.val();
        var startTag = "";
        var endTag = "";
        switch(tag) {
            case "b":
                startTag = "[b]";
                endTag = "[/b]";
                break;
            case "i":
                startTag = "[i]";
                endTag = "[/i]";
                break;            
            case "u":
                startTag = "[u]";
                endTag = "[/u]";
                break;
            case "quote":
                startTag = "[quote]";
                endTag = "[/quote]";
                break;
            case "link":
                startTag = "[url=]";
                endTag = "[/url]";
                break;
            case "image":
                startTag = "[image]";
                endTag = "[/image]";
                break;
            case "simage":
                startTag = "[simage]";
                endTag = "[/simage]";
                break;
            case "code":
                startTag = "[code]";
                endTag = "[/code]";
                break;
            case "center":
                startTag = "[center]";
                endTag = "[/center]";
                break;
            default:
        }
        
        $textArea.val(textAreaTxt.substring(0, start) + startTag + textAreaTxt.substring(start, end) + endTag + textAreaTxt.substring(end));
    });
});