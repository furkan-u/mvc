﻿using CodeKicker.BBCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Riddle.Other
{
    public static class CustomizedBBCodeParser
    {

        public static string ToHtml(string BBCode)
        {
            BBCodeParser Parser = GetParser();
            return Parser.ToHtml(BBCode);

        }
        public static BBCodeParser GetParser()
        {
            BBCodeParser Parser = new BBCodeParser(ErrorMode.ErrorFree, null, new[]
            {
                new BBTag("b", "<b>", "</b>"),
                new BBTag("i", "<span style=\"font-style:italic;\">", "</span>"),
                new BBTag("u", "<span style=\"text-decoration:underline;\">", "</span>"),
                new BBTag("code", "<pre class=\"prettyprint\">", "</pre>"){ StopProcessing = true, SuppressFirstNewlineAfter = true },
                new BBTag("image", "<img src=\"${content}\" />", "", false, true),
                new BBTag("simage", "<a target=\"_blank\" href=\"${content}\"><img border=\"0\" class=\"img-responsive simage\" src=\"${content}\">", "</a>", false,true),
                new BBTag("center", "<p class=\"text-center\">", "</p>"),
                new BBTag("quote", "<blockquote>", "</blockquote>"){ SuppressFirstNewlineAfter = true },
		            // Or if you want attribution on your quotes, you might try:
		            // new BBTag("quote", "<blockquote><span class=\"attribution\">${name}</span>", "</blockquote>"){ GreedyAttributeProcessing = true },
		        new BBTag("list", "<ul>", "</ul>"){ SuppressFirstNewlineAfter = true },
                new BBTag("*", "<li>", "</li>", true, false), 
		        // Anladığım kadarıyla BB atribute constructor'ında name kısmı boş kalırsa attribute değil de taglar arasındaki değer olduğunu belirtiyor.
		        //
		        new BBTag("url", "<a href=\"${href}\">", "</a>", new BBAttribute("href", ""), new BBAttribute("href", "href")),
                
          });
            return Parser;
        }



    }
}